﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Celulas.Demo.EF.Domain.Entities
{
    public enum ETransactionType : byte
    {
        [Display(Name = "Income")]
        INGRESO = 1,
        [Display(Name = "Expense")]
        GASTO = 2
    }
}
