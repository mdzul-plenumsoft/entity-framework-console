﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Celulas.Demo.EF.Domain.Entities
{
    public abstract class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? Id { get; set; }
        [Required]
        public DateTime? Date { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        [Required]
        public decimal? Amount { get; set; }

        [NotMapped]
        public virtual ETransactionType? Type { get; }
        [NotMapped]
        public virtual string Tag { get; }
    }
    public class IncomeTransaction : Transaction
    {
        public override ETransactionType? Type => ETransactionType.INGRESO;
        public override string Tag
        {
            get
            {
                if (this.Source == null)
                    return null;

                var s = this.Source.Description != null ? this.Source.Description : "";
                if (!string.IsNullOrEmpty(this.SourceComments) && !string.IsNullOrWhiteSpace(this.SourceComments))
                    s = string.Format("{0} ({1})", s, this.SourceComments);
                return s;
            }
        }

        [Required]
        public int? SourceId { get; set; }
        public virtual IncomeSource Source { get; set; }
        public string SourceComments { get; set; }
    }
    public class ExpenseTransaction : Transaction
    {
        public override ETransactionType? Type => ETransactionType.GASTO;
        public override string Tag => this.Category != null ? this.Category.Name : null;

        [Required]
        public int? CategoryId { get; set; }
        public virtual ExpenseCategory Category { get; set; }
    }
}
