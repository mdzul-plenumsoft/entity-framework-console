﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Celulas.Demo.EF.Domain.Entities
{
    public class ExpenseCategory
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        [StringLength(250)]
        public string Name { get; set; }
    }
}
