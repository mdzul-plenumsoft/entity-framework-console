﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Celulas.Demo.EF.Data.Repositories;
using Celulas.Demo.EF.Domain.Entities;

namespace Celulas.Demo.EF.Services
{
    public class TransactionCtrl
    {
        private readonly TransactionRepository _repository;

        public TransactionCtrl()
        {
            this._repository = new TransactionRepository();
        }
        public TransactionCtrl(TransactionRepository repository)
        {
            this._repository = repository;
        }

        public List<Transaction> Get(DateTime? from = null, DateTime? to = null)
        {
            return this._repository.Get(null, from, to).OrderByDescending(p => p.Date).ToList();
        }
        public Transaction GetById(int id)
        {
            var result = this._repository.Get(id, null, null);
            if (result.Count != 1)
                return null;
            return result.First();
        }
        public decimal GetCurrentBalance()
        {
            var result = this._repository.Get(null, null, null);
            return result.Sum(p => p.Type == ETransactionType.GASTO ? -p.Amount : p.Amount) ?? 0;
        }

        public Transaction InsertIncome(decimal amount, string description, int sourceId, string sourceComments)
        {
            #region Validaciones
            #endregion

            var entity = new IncomeTransaction();
            entity.Date = DateTime.Now;
            entity.Amount = amount;
            entity.Description = description;
            entity.SourceId = sourceId;
            entity.SourceComments = sourceComments;

            this._repository.Insert(entity);

            return entity;
        }
        public Transaction InsertExpense(decimal amount, string description, int? categoryId)
        {
            #region Validaciones
            #endregion

            var entity = new ExpenseTransaction();
            entity.Date = DateTime.Now;
            entity.Amount = amount;
            entity.Description = description;
            entity.CategoryId = categoryId;

            this._repository.Insert(entity);

            return entity;
        }
    }
}
