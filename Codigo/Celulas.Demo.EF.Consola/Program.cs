﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Celulas.Demo.EF.Services;
using Celulas.Demo.EF.Domain.Entities;

namespace Celulas.Demo.EF.Consola
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("HOLA");

            var transactionCtrl = new TransactionCtrl();
            var transactions = transactionCtrl.Get();
            foreach (var transaction in transactions) {
                var s = string.Format("{0:yyyy-MM-dd HH:mm}\t{1:C}\t\t\t{2}", transaction.Date, transaction.Amount, transaction.Tag);
                Console.WriteLine(s);
            }
            var balance = transactionCtrl.GetCurrentBalance();
            Console.WriteLine(string.Format("Tu balance actual es de: {0:C}", balance));

            Console.WriteLine("ADIOS");
            Console.ReadKey();
        }
    }
}
