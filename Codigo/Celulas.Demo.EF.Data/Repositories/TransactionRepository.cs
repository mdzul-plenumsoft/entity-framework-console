﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Celulas.Demo.EF.Domain.Entities;

namespace Celulas.Demo.EF.Data.Repositories
{
    public class TransactionRepository
    {
        private DemoEFContext _context;
        public TransactionRepository()
        {
            this._context = new DemoEFContext();
        }
        public TransactionRepository(DemoEFContext context)
        {
            this._context = context;
        }

        public List<Transaction> Get(int? id, DateTime? from, DateTime? to)
        {
            return this._context.Transactions.Where(c => (id == null || c.Id == id) && (from == null || c.Date >= from) && (to == null || c.Date <= to)).ToList();
        }

        public void Insert(Transaction transaction)
        {
            this._context.Transactions.Add(transaction);
            this._context.SaveChanges();
        }
    }
}
