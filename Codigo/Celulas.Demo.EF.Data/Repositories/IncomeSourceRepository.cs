﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Celulas.Demo.EF.Domain.Entities;

namespace Celulas.Demo.EF.Data.Repositories
{
    public class IncomeSourceRepository
    {
        private DemoEFContext _context;
        public IncomeSourceRepository(DemoEFContext context)
        {
            this._context = context;
        }

        public List<IncomeSource> Get(int? id, string description)
        {
            return this._context.IncomeSources.Where(c => (id == null || c.Id == id) && (string.IsNullOrEmpty(description) || string.IsNullOrWhiteSpace(description) || c.Description.Contains(description))).ToList();
        }

        public void Insert(IncomeSource source)
        {
            this._context.IncomeSources.Add(source);
            this._context.SaveChanges();
        }
    }
}
