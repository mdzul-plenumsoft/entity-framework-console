﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Celulas.Demo.EF.Domain.Entities;

namespace Celulas.Demo.EF.Data.Repositories
{
    public class ExpenseCategoryRepository
    {
        private DemoEFContext _context;
        public ExpenseCategoryRepository(DemoEFContext context)
        {
            this._context = context;
        }

        public List<ExpenseCategory> Get(int? id, string name)
        {
            return this._context.ExpenseCategories.Where(c => (id == null || c.Id == id) && (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name) || c.Name.Contains(name))).ToList();
        }

        public void Insert(ExpenseCategory category)
        {
            this._context.ExpenseCategories.Add(category);
            this._context.SaveChanges();
        }
    }
}
