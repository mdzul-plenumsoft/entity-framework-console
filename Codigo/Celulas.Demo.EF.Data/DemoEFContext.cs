﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Validation;
using Celulas.Demo.EF.Domain.Entities;

namespace Celulas.Demo.EF.Data
{
    public partial class DemoEFContext : DbContext
    {
        public DemoEFContext() : base("name=DemoEF")
        {
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public virtual DbSet<IncomeSource> IncomeSources { get; set; }
        public virtual DbSet<ExpenseCategory> ExpenseCategories { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IncomeSource>().ToTable("IncomeSources");
            modelBuilder.Entity<ExpenseCategory>().ToTable("ExpenseCategories");
            modelBuilder.Entity<Transaction>().ToTable("Transactions");
            modelBuilder.Entity<IncomeTransaction>().HasRequired(x => x.Source).WithMany().HasForeignKey(x => x.SourceId);
            modelBuilder.Entity<ExpenseTransaction>().HasRequired(x => x.Category).WithMany().HasForeignKey(x => x.CategoryId);
        }
    }
}
